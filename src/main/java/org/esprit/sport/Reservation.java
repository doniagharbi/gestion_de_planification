package org.esprit.sport;

import org.esprit.sport.entities.Equipement;

import java.util.Date;


/**
 * Created by Skander on 06/10/2017.
 */
public class Reservation {
    //skander cherif
    public int ref_reservation;
    public Date date_reservation;
    private Abonne abonne;
    private Equipement equipement;

    public Equipement getEquipement() {
        return equipement;
    }

    public void setEquipement(Equipement equipement) {
        this.equipement = equipement;
    }

    public int getRef_reservation() {
        return ref_reservation;
    }

    public void setRef_reservation(int ref_reservation) {
        this.ref_reservation = ref_reservation;
    }

    public Date getDate_reservation() {
        return date_reservation;
    }

    public void setDate_reservation(Date date_reservation) {
        this.date_reservation = date_reservation;
    }

    public Abonne getAbonne() {
        return abonne;
    }

    public void setAbonne(Abonne abonne) {
        this.abonne = abonne;
    }
}
